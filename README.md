# myproject



## TP

this 'TP' is meant to train and to familiarize yourself with git and its commands as well as gitlab/github.
the work requested is :

### 1. Create New Directory
create an empty folder/directory.
### 2. Navigate to Working Directory
open terminal and change working directory to the one you just created.
### 3. Clone
clone this repository using the command : 
``` 
git clone <repo-url>

```
change < repo-url > with the HTTPS value u get when you click on the clone drop-down (blue button) located in the top.
### 4. Make Changes
open vs code and create a new txt file named after you, like : fullname.txt.
### 5. Stage Changes
stage your changes using the command : 
```
git add .
```
### 6. Commit
commit your changes using : 
```
git commit -m 'commit message'
```

### 7. Push
push your local changes to the remote repo using :
```
git push origin main
```
you may have to enter your account's username and password. after doing so your commit will be pushed.

